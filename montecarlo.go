/**
 * Auteur: P-Alain Curty
 * Sujet: Méthode d'approximation aléatoire de Montecarlo. (Préréglée pour PI)
 * Options (flags) : -n = Nombre de fléchettes à tirer aléatoirement
 * [Command-Line]  : -threads = Nombre de threads à utiliser
 */
package main

import (
	"flag"
	"fmt"
	"math/rand"
	"sync"
	"time"
)

// Constantes par défaut
const DEF_N int64 = 1000000000
const DEF_NB_THREADS int64 = 4

// Fonction de calcul pour PI
func f(x float32, y float32) int64 {
	if x*x+y*y <= 1 {
		return 1
	}
	return 0
}

// Fonction parallèlisée (tirage aléatoire et incrémentation)
func Montecarlo(fct func(float32, float32) int64, c chan int64, SUB_N int64, wg *sync.WaitGroup) {
	defer wg.Done() // On indique qu'à la fin du traitement, on signal le thread comme terminé
	generator := rand.New(rand.NewSource(time.Now().UnixNano()))
	var nb int64 = 0
	for i := int64(0); i < SUB_N; i++ {
		x := generator.Float32()
		y := generator.Float32()
		nb += fct(x, y)
	}
	c <- nb // On transmet le résultat dans le channel
}

func main() {
	// Options du programme
	var N int64
	var NB_THREADS int64
	flag.Int64Var(&N, "n", DEF_N, "Number of darts in the system")
	flag.Int64Var(&NB_THREADS, "threads", DEF_NB_THREADS, "Number of goroutine used")
	flag.Parse()

	// Initialisations
	var count int64 = 0
	c := make(chan int64)
	var wg sync.WaitGroup
	wg.Add(int(NB_THREADS))

	// Démarrage du programme
	fmt.Printf("Start Montecarlo (%d flechettes sur %d threads).\n", N, NB_THREADS)
	start := time.Now()

	for i := int64(0); i < NB_THREADS; i++ {
		var nFrac int64
		if nFrac = (N / NB_THREADS) + (N % NB_THREADS); i < (NB_THREADS - 1) {
			nFrac = int64(N / NB_THREADS)
		}
		go Montecarlo(f, c, nFrac, &wg) // Démarrage des goroutines !
	}

	// Fonction d'addition. Parallèlisé également. Additionne lorsque des valeurs
	// sont ajoutée au channel
	go func() {
		defer wg.Done()
		for j := range c {
			count += j
		}
	}()

	wg.Wait() // On attend que tous les threads soient finis

	end := time.Since(start)
	fmt.Printf("Montecarlo fini en %f sec.\n", end.Seconds())

	// Affichage + calcul final
	var piHat float32 = float32(4) * float32(count) / float32(N)
	fmt.Printf("Approximation de PI pour %d/%d flechettes dans la zone: %f.\n", count, N, piHat)
}
