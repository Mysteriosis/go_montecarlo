# go_montecarlo
Paralellized Montecarlo method coded in Go! to compute PI.

## Installation
1. Go to your **$GOPATH**
2. Copy montecarlo.go to a specified folder (ex. $GOPATH/github.com/user/montecarlo/)
3. Run ```go install github.com/user/montecarlo```

## Options
Options (Flags Command-Line) :
-n = Nombre de fléchettes à tirer aléatoirement
-threads = Nombre de threads à utiliser
